# Repo for Configs for the LHCbUpgrade2StackTester

Before you can create configs, please do the following:

1. Request access to this repository
2. Clone the repo
3. If this is a new request, create a new branch following the naming convention `<username>_<project>`. If it's a continuation/alteration of a previous request, switch to that branch
4. Using the `StackCreator.json` and `TestJobSchedule.json` files as examples, create one or more JSON files in the appropriate folders. Note only the root of these folders will be scanned for new configs.
5. Any required Job Options should be placed in the folder within the `JobsConfig` folder, including any folder heirachy required.
6. Push your branch
7. Create a merge request against `master` branch


